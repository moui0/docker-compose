# Maintainer: Andrew Crerar <crerar@archlinux.org>
# Maintainer: Morten Linderud <foxboron@archlinux.org>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: Iwan Timmer <irtimmer@gmail.com>
# Contributor: Vincent Demeester <vincent@sbr.io>
# Contributor: Josh VanderLinden <arch@cloudlery.com>

pkgname=docker-compose
pkgver=2.24.6
pkgrel=1
pkgdesc="Fast, isolated development environments using Docker"
arch=('x86_64')
url="https://www.docker.com/"
license=("Apache-2.0")
makedepends=('go')
checkdepends=('docker')
source=("$pkgname-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz")
b2sums=('a2386be9031fdd27f670858070731017d7f6d8318aa02650a8ce5ec1b08d5f1e46cf80a352d7067cb64b8f3647fcd4ba863ce7477b88745df3720892b3083201')

build() {
  cd "compose-$pkgver"
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -ldflags=-linkmode=external -ldflags=-X=github.com/docker/compose/v2/internal.Version=${pkgver} -mod=readonly -modcacherw"
  go build -trimpath -tags "e2e,kube" -o compose ./cmd
}

check(){
  cd "compose-$pkgver"
  rm pkg/compose/ps_test.go
  go test -tags "e2e,kube" -v $(go list -tags "e2e,kube" ./... | grep -vE 'e2e')
}

package() {
  cd "compose-$pkgver"
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
  install -Dm755 compose "$pkgdir"/usr/lib/docker/cli-plugins/docker-compose
  install -d "$pkgdir/usr/bin"
  ln -sf /usr/lib/docker/cli-plugins/docker-compose "$pkgdir/usr/bin/docker-compose"
}
